module git.nemunai.re/youp0m

go 1.16

require (
	github.com/aws/aws-sdk-go v1.44.136
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	gitlab.com/nyarla/go-crypt v0.0.0-20160106005555-d9a5dc2b789b
)
