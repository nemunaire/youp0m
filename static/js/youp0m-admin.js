api_url = "/api/next/";
img_url = "/images/next/";

function publish_pict(pict) {
  fetch(api_url + "/" + pict.name + "/publish").then(response => {
      picts = null;
      sync()
  });
}

function delete_pict(pict) {
  fetch(api_url + "/" + pict.name, { method: "DELETE" }).then(response => {
      picts = null;
      sync()
  });
}

function admin_ctrl(pict) {
    var figure = document.createElement("figure");
    figure.className = "moz";
    var img = document.createElement("img");
    img.src = img_url + pict.name;
    img.alt = pict.name;
    var div = document.createElement("div");
    div.className = "menu"
    var linkPub = document.createElement("a");
    linkPub.onclick = function(e) {
        publish_pict(pict);
        return false;
    };
    linkPub.href = "#";
    linkPub.innerHTML = "Publier";
    var linkDel = document.createElement("a");
    linkDel.onclick = function(e) {
        delete_pict(pict);
        return false;
    };
    linkDel.href = "#";
    linkDel.innerHTML = "Supprimer";
    div.appendChild(linkPub);
    div.appendChild(linkDel);
    figure.appendChild(div);
    figure.appendChild(img);
    document.body.appendChild(figure);
}

function sync() {
    path = window.location.pathname.replace(/.*\/([^\/]*)/, "$1");
    if (path == "" || path == "all")
	show_mozaic(admin_ctrl);
    else
	show_picture(path);
}
