//go:build !dev
// +build !dev

package main

import (
	"embed"
	"io/fs"
	"log"
	"net/http"
)

//go:embed static/* static/js/* static/css/*
var _assets embed.FS

var Assets http.FileSystem

func init() {
	sub, err := fs.Sub(_assets, "static")
	if err != nil {
		log.Fatal("Unable to cd to static/ directory:", err)
	}
	Assets = http.FS(sub)
}

func sanitizeStaticOptions() error {
	return nil
}
