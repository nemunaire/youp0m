package main

import (
	"errors"
	"io"
)

func ApiImagesRouting(pe *PictureExplorer) map[string]struct {
	AuthFunction
	DispatchFunction
} {
	return map[string]struct {
		AuthFunction
		DispatchFunction
	}{
		"GET":    {PublicPage, pe.listImages},
		"POST":   {PublicPage, pe.addImage},
		"DELETE": {PrivatePage, pe.hideImage},
	}
}

func ApiNextImagesRouting(pe *PictureExplorer) map[string]struct {
	AuthFunction
	DispatchFunction
} {
	return map[string]struct {
		AuthFunction
		DispatchFunction
	}{
		"GET":    {PrivatePage, pe.listNextImages},
		"POST":   {PublicPage, pe.addImage},
		"DELETE": {PrivatePage, pe.deleteImage},
	}
}

func (pe *PictureExplorer) listImages(u *User, args []string, body io.ReadCloser) (interface{}, error) {
	if len(args) < 1 {
		return pe.GetPublishedImages()
	} else if args[0] == "last" {
		return pe.GetLastImage()
	} else {
		return pe.GetPublishedImage(args[0])
	}
}

func (pe *PictureExplorer) listNextImages(u *User, args []string, body io.ReadCloser) (interface{}, error) {
	if len(args) < 1 {
		return pe.GetNextImages()
	} else if len(args) >= 2 && args[1] == "publish" {
		if pict, err := pe.GetNextImage(args[0]); err != nil {
			return nil, err
		} else if err := pe.Publish(pict); err != nil {
			return nil, err
		} else {
			return true, nil
		}
	} else {
		return pe.GetNextImage(args[0])
	}
}

func (pe *PictureExplorer) addImage(u *User, args []string, body io.ReadCloser) (interface{}, error) {
	if len(args) < 1 {
		return nil, errors.New("Need an image identifier to create")
	} else if err := pe.AddImage(args[0], body); err != nil {
		return nil, err
	} else {
		return true, nil
	}
}

func (pe *PictureExplorer) hideImage(u *User, args []string, body io.ReadCloser) (interface{}, error) {
	if len(args) < 1 {
		return nil, errors.New("Need an image identifier to delete")
	} else if pict, err := pe.GetPublishedImage(args[0]); err != nil {
		return nil, errors.New("No matching image")
	} else if err := pe.Unpublish(pict); err != nil {
		return nil, err
	} else {
		return true, nil
	}
}

func (pe *PictureExplorer) deleteImage(u *User, args []string, body io.ReadCloser) (interface{}, error) {
	if len(args) < 1 {
		return nil, errors.New("Need an image identifier to delete")
	} else if pict, err := pe.GetNextImage(args[0]); err != nil {
		return nil, errors.New("No matching image")
	} else if err := pe.Remove(pict); err != nil {
		return nil, err
	} else {
		return true, nil
	}
}
