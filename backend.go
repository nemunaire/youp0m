package main

import (
	"fmt"
	"image"
	"io"
	"net/http"
	"strings"
)

var existing_backends []string

type BackendSelector string

func (s *BackendSelector) String() string {
	return string(*s)
}

func (s *BackendSelector) Set(v string) error {
	found := false
	for _, b := range existing_backends {
		if strings.ToLower(v) == b {
			found = true
			tmp := BackendSelector(v)
			s = &tmp
			break
		}
	}

	if !found {
		return fmt.Errorf("%q is not a known file backend (existing backends: %v)", v, existing_backends)
	}
	return nil
}

type FileBackend interface {
	DeletePicture(string, string) error
	ServeFile() http.Handler
	GetPicture(string, string, io.Writer) error
	GetPictureInfo(string, string) (*Picture, error)
	ListPictures(string) ([]*Picture, error)
	MovePicture(string, string, string) error
	PutPicture(string, string, *image.Image) error
}
