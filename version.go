package main

import (
	"io"
)

const VERSION = 0.3

var ApiVersionRouting = map[string]struct {
	AuthFunction
	DispatchFunction
}{
	"GET": {PublicPage, showVersion},
}

func showVersion(u *User, args []string, body io.ReadCloser) (interface{}, error) {
	m := map[string]interface{}{"version": VERSION}

	if u != nil {
		m["youare"] = *u
	}

	return m, nil
}
