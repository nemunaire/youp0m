package main

import (
	"net/http"
)

func init() {
	mux.HandleFunc("/css/", func(w http.ResponseWriter, r *http.Request) {
		http.FileServer(Assets).ServeHTTP(w, r)
	})
	mux.HandleFunc("/js/", func(w http.ResponseWriter, r *http.Request) {
		http.FileServer(Assets).ServeHTTP(w, r)
	})
}
